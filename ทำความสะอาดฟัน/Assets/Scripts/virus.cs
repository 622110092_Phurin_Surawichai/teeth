using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class virus : MonoBehaviour
{
    public List<GameObject> dirtyPlaces = new List<GameObject>();

    public Text ProgressText;

    public Slider ProgressSlider;
    

    private int objectCount=10;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CheckProgress();
    }

    private void CheckProgress()
    {
        if (dirtyPlaces.Count < objectCount)
        {
            objectCount -= 1;
            ProgressSlider.value += 10;
            ProgressText.text = ProgressSlider+"%";
        }
    }
}
