using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
public class brushSFX : MonoBehaviour
{
    public AudioSource _brushSFX;

    public float replayTimer=2.2f;

    public float Timer=0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Timer += Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (Timer >= replayTimer)
        {
            if (other.tag == ("Enemy"))
            {
                _brushSFX.Play();
                Timer = 0;
            }
        }
        
    }
}
