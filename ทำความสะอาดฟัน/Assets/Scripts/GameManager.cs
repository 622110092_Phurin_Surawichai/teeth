using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
    public bool innerIsDone = false;
    public bool outerIsDone = false;
    public bool toungeIsDone = false;

    public bool IbtnClick = false;
    public bool ObtnClick = false;
    public bool TbtnClick = false;

    public Slider ProgressSlider;

    public Button InnerButton;
    public Button OuterButton;
    public Button ToungeButton;

    public GameObject endPanel;

    private float generalTempValue;
    private float ITempValue;
    private float OTempValue;
    private float TTempValue;
    
    public Text ProgressText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        generalTempValue = ProgressSlider.value;
        CheckProgress();
        if (IbtnClick == true)
        {
            ITempValue = generalTempValue;
        }
        if (ObtnClick == true)
        {
            OTempValue = generalTempValue;
        }
        if (TbtnClick == true)
        {
            TTempValue = generalTempValue;
        }
        ProgressText.text = ProgressSlider.value+"%";
    }

    public void CheckProgress()
    {
        if (IbtnClick == true && ProgressSlider.value == 100)
        {
            innerIsDone = true;
        }

        if (ObtnClick == true && ProgressSlider.value == 100)
        {
            outerIsDone = true;
        }

        if (TbtnClick == true && ProgressSlider.value == 100)
        {
            toungeIsDone = true;
        }

        if (innerIsDone == true && outerIsDone == true && toungeIsDone == true)
        {
            endPanel.SetActive(true);
        }
    }
    public void InnerClick()
    {
        ProgressSlider.value = ITempValue;
        IbtnClick = true;
        ObtnClick = false;
        TbtnClick = false;
    }
    public void OuterClick()
    {
        ProgressSlider.value = OTempValue;
        ObtnClick = true;
        IbtnClick = false;
        TbtnClick = false;
    }
    public void ToungeClick()
    {
        ProgressSlider.value = TTempValue;
        TbtnClick = true;
        IbtnClick = false;
        ObtnClick = false;
    }
}
