using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class destroyVirus : MonoBehaviour
{
    protected int brushCount = 0;

    public Slider ProgressSlider;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == ("Player"))
        {
            brushCount += 1;
            if (brushCount >= 10)
            {
                ProgressSlider.value += 10;
                Destroy(this.gameObject);
            }
        }
        
    }
}
