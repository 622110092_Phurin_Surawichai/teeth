using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class followMouse : MonoBehaviour
{
    [SerializeField]private Camera mainCamera;

    //[SerializeField] private LayerMask _layerMask;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray,out RaycastHit raycastHit))
        {
            transform.position = new Vector3(raycastHit.point.x,raycastHit.point.y,raycastHit.point.z-0.15f);
        }
        /*if (Physics.Raycast(ray, out RaycastHit raycastHit,float.MaxValue,_layerMask))
        {
            transform.position = raycastHit.point;
        }*/
    }
}
